from my_service import read_html
from org.csstudio.display.builder.runtime.script import PVUtil
import json

u = "https://shifter.esss.lu.se/ioc-update"
keys = ["SID","SL","SLPhone","SLEmail","OP","OPPhone","OPEmail"]
prefix = "NSO:Ops:"

df = read_html(u)
data = json.loads(df)

for key in keys:
    if key not in data.keys():
        data[key] = "N/A"
    PVUtil.writePV("loc://{}{}".format(prefix,key),str(data[key]),5)
